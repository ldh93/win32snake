#pragma once

#include "publicdef.h"

class CBody
{
public:
	CBody(void);
	CBody(const SNAKE_BODY_ENUM &, const int &, const int &);
	virtual ~CBody(void);
	void OnPaint(const HDC & hdc);
	inline void SetType(const SNAKE_BODY_ENUM & type) { m_body = type; }
	inline SNAKE_BODY_ENUM GetType() { return m_body; }
	inline int GetX(){ return m_x; }
	inline void SetX(const int & x){ m_x = x; }
	inline int GetY(){ return m_y; }
	inline void SetY(const int & y){ m_y = y; }
private:
	int m_x;
	int m_y;
	SNAKE_BODY_ENUM m_body;
	COLORREF m_color;
};

