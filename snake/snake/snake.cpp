// snake.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "snake.h"
#include "MySnake.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_LOADSTRING 100

CMySnake mySnake;

// 全局变量:
HINSTANCE hInst;								// 当前实例
TCHAR szTitle[MAX_LOADSTRING];					// 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];			// 主窗口类名
bool GAME_BEGIN = true;							//游戏开始/结束
bool GAME_PAUSE = false;						//游戏暂停/继续
int FOOD_X = 0;									//食物横坐标
int FOOD_Y = 0;									//食物纵坐标
// 此代码模块中包含的函数的前向声明:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void OnPaint(HWND hWnd, HDC hdc);
VOID MYTIMERPROC(HWND, UINT, UINT_PTR, DWORD);
void BeginGame(HWND);
void OverGame(HWND);
void ContinueGame(HWND);
void PauseGame(HWND);
void MakeFood(HWND);
void DrawFood(HDC hdc);
bool CheckLife(const RECT & rect, const Point & pt);
bool EatFood(const Point & pt);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: 在此放置代码。
	MSG msg;
	HACCEL hAccelTable;

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_SNAKE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 执行应用程序初始化:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SNAKE));

	// 主消息循环:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SNAKE));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1)/*CreateSolidBrush(RGB(0, 0, 0))*/;
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_SNAKE);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // 将实例句柄存储在全局变量中

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   BeginGame(hWnd);

   srand( (unsigned int)time(NULL));

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);
   
   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND	- 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY	- 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_START:
			if (GAME_BEGIN)
			{
				OverGame(hWnd);
			}
			BeginGame(hWnd);
			break;
		case ID_PAUSE:
			if (GAME_PAUSE)
			{
				ContinueGame(hWnd);
			}
			else
			{
				PauseGame(hWnd);
			}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_UP:
				mySnake.SetDirection(up);
				break;
			case VK_DOWN:
				mySnake.SetDirection(down);
				break;
			case VK_LEFT:
				mySnake.SetDirection(left);
				break;
			case VK_RIGHT:
				mySnake.SetDirection(right);
				break;
			default:break;
			}
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此添加任意绘图代码...
		OnPaint(hWnd, hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void OnPaint(HWND hWnd, HDC hdc)
{
	if (GAME_BEGIN)
	{
		//绘图
		mySnake.OnPaint(hdc);
		DrawFood(hdc);
	}
	else
	{
		RECT rect;
		GetClientRect(hWnd, &rect);
		HBRUSH hBrush = CreateSolidBrush(RGB(255, 0, 0));//定义的画刷
		SelectObject(hdc,hBrush); //选择画刷
		DrawText(hdc, _T("游戏结束!"), -1, &rect, DT_CENTER);
		DeleteObject(hBrush); //删除画刷
	}
}

VOID MYTIMERPROC(HWND hWnd, UINT message, UINT_PTR event, DWORD dwTime)
{
	switch(event)
	{
	case TIMER_MOVE:
		Point pt = mySnake.Move();
		if (EatFood(pt))
		{
			MakeFood(hWnd);
			mySnake.AddTail();
		}
		//mySnake.AddHead();
		RECT rect;
		GetClientRect(hWnd,&rect);
		if (!CheckLife(rect, pt))
		{
			OverGame(hWnd);
		}
		InvalidateRect(hWnd,&rect,TRUE);  
		UpdateWindow(hWnd);  
		break;
	}
}

void BeginGame(HWND hWnd)
{
	mySnake.OnCreate();
	GAME_BEGIN = true;
	GAME_PAUSE = false;
	MakeFood(hWnd);
	SetTimer(hWnd, TIMER_MOVE, 150, (TIMERPROC)MYTIMERPROC);
}
void OverGame(HWND hWnd)
{
	GAME_BEGIN = false;
	KillTimer(hWnd, TIMER_MOVE);
}

void ContinueGame(HWND hWnd)
{
	GAME_PAUSE = false;
	SetTimer(hWnd, TIMER_MOVE, 150, (TIMERPROC)MYTIMERPROC);
}

void PauseGame(HWND hWnd)
{
	GAME_PAUSE = true;
	KillTimer(hWnd, TIMER_MOVE);
}

void MakeFood(HWND hWnd)
{
	do 
	{
		rand();
		RECT rect;
		GetClientRect(hWnd, &rect);
		FOOD_X = rand();
		FOOD_X = FOOD_X % (rect.right-rect.left);
		FOOD_X = (FOOD_X / 10) * 10;
		FOOD_Y = rand();
		FOOD_Y = FOOD_Y % (rect.bottom-rect.top);
		FOOD_Y = (FOOD_Y / 10) * 10;
	} while (!mySnake.CheckFoodPos(FOOD_X, FOOD_Y));
}

void DrawFood(HDC hdc)
{
	HBRUSH hBrush = CreateSolidBrush(RGB(255, 255, 0));//定义的画刷
	SelectObject(hdc,hBrush); //选择画刷
	Rectangle(hdc, FOOD_X, FOOD_Y, FOOD_X+10, FOOD_Y+10);
	DeleteObject(hBrush); //删除画刷
}

bool CheckLife(const RECT & rect, const Point & pt)
{
	if ( (pt.X >= (rect.right - rect.left)) || (pt.Y >= (rect.bottom - rect.top)) || (pt.X < 0) || (pt.Y < 0) )
	{
		return false;
	}

	return mySnake.CheckLife();
}

bool EatFood(const Point & pt)
{
	bool bRet = false;
	if (pt.X == FOOD_X && pt.Y == FOOD_Y)
	{
		bRet = true;
	}

	return bRet;
}