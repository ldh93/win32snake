#pragma once

#include <list>
#include "Body.h"
#include <GdiplusEnums.h>
#include <GdiplusTypes.h>

class CMySnake
{
public:
	CMySnake(void);
	virtual ~CMySnake(void);
	void OnCreate();
	void Destroy();
	void OnPaint(const HDC & hdc);
	void AddHead();
	void AddBody();
	void AddTail();
	inline void SetDirection(const SNAKE_DIRECTION & dir){ 
		switch (m_Direction)
		{
		case up:
		case down:
			if (up != dir && down != dir)
			{
				m_Direction = dir;
			}
			break;
		case left:
		case right:
			if (left != dir && right != dir)
			{
				m_Direction = dir;
			}
			break;
		default:break;
		}
	}
	Point Move();
	Point MoveUp();
	Point MoveDown();
	Point MoveLeft();
	Point MoveRight();
	bool CheckFoodPos(const int & x, const int & y);
	bool CheckLife();
private:
	SNAKE_DIRECTION m_Direction;
	std::list<CBody*> m_snakeList;
};

