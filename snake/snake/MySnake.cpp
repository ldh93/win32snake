#include "StdAfx.h"
#include "MySnake.h"


CMySnake::CMySnake(void) : m_Direction(right)
{
}


CMySnake::~CMySnake(void)
{
	Destroy();
}

void CMySnake::OnCreate()
{
	//先创建一个头部
	CBody *pHead = new CBody(head, 20, 0);
	m_snakeList.push_front(pHead);
	//创建一个身体
	CBody *pBody = new CBody(body, 10, 0);
	m_snakeList.push_back(pBody);
	//创建一个尾部
	CBody *pTail = new CBody(tail, 0, 0);
	m_snakeList.push_back(pTail);
}

void CMySnake::Destroy()
{
	std::list<CBody*>::const_iterator iter = m_snakeList.begin();
	for (iter; m_snakeList.end() != iter; ++iter)
	{
		CBody* pBody = *iter;
		if (pBody)
		{
			delete pBody;
			pBody = nullptr;
		}
	}
}

void CMySnake::OnPaint(const HDC & hdc)
{
	std::list<CBody*>::const_iterator iter = m_snakeList.begin();
	for (;m_snakeList.end() != iter ; ++iter)
	{
		CBody* pBody = *iter;
		pBody->OnPaint(hdc);
	}
}

void CMySnake::AddHead()
{
	std::list<CBody*>::const_iterator iter = m_snakeList.begin();
	if (m_snakeList.end() != iter)
	{
		CBody* pBody = *iter;
		if (pBody)
		{
			pBody->SetType(body);
			int x = 0, y = 0;
			switch(m_Direction)
			{
			case up:
				x = pBody->GetX();
				y = pBody->GetY() - 10;
				break;
			case down:
				x = pBody->GetX();
				y = pBody->GetY() + 10;
				break;
			case left:
				x = pBody->GetX() - 10;
				y = pBody->GetY();
				break;
			case right:
				x = pBody->GetX() + 10;
				y = pBody->GetY();
				break;
			default:
				break;
			}
			CBody *pHead = new CBody(head, x, y);
			m_snakeList.push_front(pHead);
		}
	}
	else
	{
		CBody *pHead = new CBody(head, 0, 0);
		m_snakeList.push_front(pHead);
	}
}

void CMySnake::AddBody()
{
	AddHead();
}

void CMySnake::AddTail()
{
	CBody *pBody = m_snakeList.back();
	if (pBody)
	{
		if (pBody)
		{
			pBody->SetType(body);
			int x = 0, y = 0;
			switch(m_Direction)
			{
			case up:
				x = pBody->GetX();
				y = pBody->GetY() + 10;
				break;
			case down:
				x = pBody->GetX();
				y = pBody->GetY() - 10;
				break;
			case left:
				x = pBody->GetX() + 10;
				y = pBody->GetY();
				break;
			case right:
				x = pBody->GetX() - 10;
				y = pBody->GetY();
				break;
			default:
				break;
			}
			CBody *pHead = new CBody(tail, x, y);
			m_snakeList.push_back(pHead);
		}
	}
}

Point CMySnake::Move()
{
	Point pt(0, 0);
	switch (m_Direction)
	{
	case up:
		pt = MoveUp();
		break;
	case down:
		pt = MoveDown();
		break;
	case left:
		pt = MoveLeft();
		break;
	case right:
		pt = MoveRight();
		break;
	default:
		break;
	}
	return pt;
}

Point CMySnake::MoveUp()
{
	Point pt(0, 0);
	std::list<CBody*>::iterator iter = m_snakeList.end();
	--iter;
	while (1)
	{
		if (iter == m_snakeList.begin())
		{
			CBody *pHead = *iter;
			pHead->SetY(pHead->GetY() - 10);
			pt.X = pHead->GetX();
			pt.Y = pHead->GetY();
			break;
		}
		CBody *pBody = *iter;
		if (pBody)
		{
			--iter;
			CBody *pBodyPre = *iter;
			if (pBodyPre)
			{
				pBody->SetX(pBodyPre->GetX());
				pBody->SetY(pBodyPre->GetY());
			}
		}
	}

	return pt;
}

Point CMySnake::MoveDown()
{
	Point pt(0, 0);
	std::list<CBody*>::iterator iter = m_snakeList.end();
	--iter;
	while (1)
	{
		if (iter == m_snakeList.begin())
		{
			CBody *pHead = *iter;
			pHead->SetY(pHead->GetY() + 10);
			pt.X = pHead->GetX();
			pt.Y = pHead->GetY();
			break;
		}
		CBody *pBody = *iter;
		if (pBody)
		{
			--iter;
			CBody *pBodyPre = *iter;
			if (pBodyPre)
			{
				pBody->SetX(pBodyPre->GetX());
				pBody->SetY(pBodyPre->GetY());
			}
		}
	}

	return pt;
}

Point CMySnake::MoveLeft()
{
	Point pt(0, 0);
	std::list<CBody*>::iterator iter = m_snakeList.end();
	--iter;
	while (1)
	{
		if (iter == m_snakeList.begin())
		{
			CBody *pHead = *iter;
			pHead->SetX(pHead->GetX()-10);
			pt.X = pHead->GetX();
			pt.Y = pHead->GetY();
			break;
		}
		CBody *pBody = *iter;
		if (pBody)
		{
			--iter;
			CBody *pBodyPre = *iter;
			if (pBodyPre)
			{
				pBody->SetX(pBodyPre->GetX());
				pBody->SetY(pBodyPre->GetY());
			}
		}
	}

	return pt;
}

Point CMySnake::MoveRight()
{
	Point pt(0, 0);
	std::list<CBody*>::iterator iter = m_snakeList.end();
	--iter;
	while (1)
	{
		if (iter == m_snakeList.begin())
		{
			CBody *pHead = *iter;
			pHead->SetX(pHead->GetX()+10);
			pt.X = pHead->GetX();
			pt.Y = pHead->GetY();
			break;
		}
		CBody *pBody = *iter;
		if (pBody)
		{
			--iter;
			CBody *pBodyPre = *iter;
			if (pBodyPre)
			{
				pBody->SetX(pBodyPre->GetX());
				pBody->SetY(pBodyPre->GetY());
			}
		}
	}

	return pt;
}

bool CMySnake::CheckFoodPos(const int & x, const int & y)
{
	bool bRet = true;
	std::list<CBody*>::iterator iter = m_snakeList.begin();
	for (iter; m_snakeList.end() != iter; ++iter)
	{
		CBody* pBody = *iter;
		if (pBody)
		{
			if (pBody->GetX()==x && pBody->GetY()==y)
			{
				bRet = false;
				break;
			}
		}
	}

	return bRet;
}

bool CMySnake::CheckLife()
{
	bool bRet = true;
	std::list<CBody*>::iterator iter = m_snakeList.begin();
	Point pt;
	for (iter; m_snakeList.end() != iter; ++iter)
	{
		if (m_snakeList.begin() != iter)
		{
			CBody* pBody = *iter;
			if (pBody)
			{
				if (pBody->GetX()==pt.X && pBody->GetY()==pt.Y)
				{
					bRet = false;
					break;
				}
			}
		}
		else
		{
			CBody* pBody = *iter;
			if (pBody)
			{
				pt.X = pBody->GetX();
				pt.Y = pBody->GetY();
			}
		}
	}

	return bRet;
}