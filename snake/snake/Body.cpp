#include "StdAfx.h"
#include "Body.h"


CBody::CBody(void)
	:m_x(0)
	,m_y(0)
	,m_body(head)
	,m_color(0)
{
}

CBody::CBody(const SNAKE_BODY_ENUM & body, const int & x, const int & y)
	:m_x(x)
	,m_y(y)
	,m_body(body)
	,m_color(0)
{

}

CBody::~CBody(void)
{
}

void CBody::OnPaint(const HDC & hdc)
{
	switch (m_body)
	{
	case head:
		{
			m_color = RGB(255, 105, 180);
			HBRUSH hBrush = CreateSolidBrush(m_color);//����Ļ�ˢ
			SelectObject(hdc,hBrush); //ѡ��ˢ
			Rectangle(hdc, m_x, m_y, m_x+10, m_y+10);
			DeleteObject(hBrush); //ɾ����ˢ
		}
		break;
	case body:
		{
			m_color = RGB(0, 0, 255);
			HBRUSH hBrush = CreateSolidBrush(m_color);
			SelectObject(hdc,hBrush);
			Rectangle(hdc, m_x, m_y, m_x+10, m_y+10);
			DeleteObject(hBrush);
		}
		break;
	case tail:
		{
			m_color = RGB(0, 255, 0);
			HBRUSH hBrush = CreateSolidBrush(m_color);
			SelectObject(hdc,hBrush);
			Rectangle(hdc, m_x, m_y, m_x+10, m_y+10);
			DeleteObject(hBrush);
		}
		break;
	default:
		break;
	}
}